from rest_framework import routers, serializers, viewsets
from .core import models


class ProdutoSerializer(serializers.HyperlinkedModelSerializer):
    marca = serializers.StringRelatedField()

    class Meta:
        model = models.Produto
        fields = ('id',
                  'nome',
                  'marca',
                  'fabricante',
                  'preco',
                  'n_vitorias',
                  'media_nossa',
                  'media_concorrencia',
                  'diferenca',
                  'incidencia',
                  'n_vitorias', )


class ProdutoViewSet(viewsets.ModelViewSet):
    queryset = models.Produto.objects.all()
    serializer_class = ProdutoSerializer

    def get_queryset(self):
        qs = super(ProdutoViewSet, self).get_queryset()
        qs = qs.apenas_nossos()
        return qs.order_by('nome')


class MarcaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Marca
        fields = '__all__'


class MarcaViewSet(viewsets.ModelViewSet):
    queryset = models.Marca.objects.all()
    serializer_class = MarcaSerializer


class FabricanteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Fabricante
        fields = '__all__'


class FabricanteViewSet(viewsets.ModelViewSet):
    queryset = models.Fabricante.objects.all()
    serializer_class = FabricanteSerializer


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'produtos', ProdutoViewSet)
router.register(r'marcas', MarcaViewSet)
router.register(r'fabricantes', FabricanteViewSet)
