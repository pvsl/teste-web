from django.shortcuts import render
from django.views.generic import TemplateView


class PerformanceMonitor(TemplateView):
    template_name = 'core/performancemonitor.html'


performancemonitor_view = PerformanceMonitor.as_view()
