from django.db import models
from django.utils.functional import cached_property
from django.db.models import Avg


class Fabricante(models.Model):
    nome = models.CharField('Nome', max_length=50)

    def __str__(self):
        return '{}'.format(self.nome)


class Marca(models.Model):
    nome = models.CharField('Nome', max_length=50)
    fabricante = models.ForeignKey('core.Fabricante', on_delete=models.PROTECT)

    def __str__(self):
        return '{}'.format(self.nome)


class ProdutoQuerySet(models.QuerySet):
    def concorrentes(self):
        return self.filter(is_concorrente=True)

    def apenas_nossos(self):
        return self.filter(is_concorrente=False)


class ProdutoManager(models.Manager):
    def concorrentes(self):
        return self.get_queryset().concorrentes()

    def apenas_nossos(self):
        return self.get_queryset().apenas_nossos()

    def get_queryset(self):
        return ProdutoQuerySet(self.model, using=self._db)


class Produto(models.Model):
    marca = models.ForeignKey('core.Marca', on_delete=models.PROTECT)
    nome = models.CharField(verbose_name='Nome', max_length=50)
    preco = models.DecimalField(verbose_name='Preço',
                                max_digits=10,
                                decimal_places=3)
    is_concorrente = models.BooleanField(verbose_name='Concorrente',
                                         default=True)
    relacionados = models.ManyToManyField('self', verbose_name='Relacionados')

    @cached_property
    def fabricante(self):
        return '{}'.format(self.marca.fabricante)

    @cached_property
    def n_vitorias(self):
        return self.concorrentes.filter(preco__lt=self.preco).count()

    @cached_property
    def incidencia(self):
        return self.concorrentes.all().count()

    @cached_property
    def diferenca(self):
        if self.media_nossa and self.media_nossa != 0:
            return int(self.media_concorrencia * 100 / self.media_nossa) - 100

        return 0

    @cached_property
    def media_nossa(self):
        return self.relacionados.exclude(
            is_concorrente=True).aggregate(Avg('preco'))['preco__avg'] or 0

    @cached_property
    def media_concorrencia(self):
        return self.concorrentes.aggregate(Avg('preco'))['preco__avg'] or 0

    @cached_property
    def concorrentes(self):
        return self.relacionados.concorrentes()

    def __str__(self):
        return '{}'.format(self.nome)

    objects = ProdutoManager()
