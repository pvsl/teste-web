var path = require("path");
var webpack = require('webpack');
var BundleTracker = require('webpack-bundle-tracker');
var MiniCssExtractPlugin = require("mini-css-extract-plugin");

const config = {
  assetsDir: path.resolve('./assets'),
  outputPath: path.resolve('/opt/django/static-data/static/dist/')
};

module.exports = {
  context: __dirname,
  mode: 'production',
  entry: [
    path.join(config.assetsDir, 'app.js')
  ],
  output: {
    path: config.outputPath,
    filename: "[name].js",
  },
  externals: {
    jquery: 'jQuery'
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          "sass-loader",
        ]
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'fonts/',
            publicPath: '../'
          }
        }]
      },
      {
        test: /.(png|jpg|ico)$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'images/'
          }
        }]
      },
    ],
  },
  plugins: [
    new BundleTracker({filename: './webpack-stats.json'}),
    new MiniCssExtractPlugin({
      outputPath: 'css/',
      filename: "css/[name].css",
      chunkFilename: "[id].css"
    })
  ],
  resolve: {
    modules: ['node_modules'],
    extensions: ['.js', '.jsx']
  },
};
