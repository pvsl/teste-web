import 'jquery/jquery.min.js';
import 'bootstrap-sass/assets/javascripts/bootstrap.min.js';

import 'datatables/media/js/jquery.dataTables.min.js';

import 'datatables/media/css/jquery.dataTables.min.css';
import './fontawesome.scss';
import './bootstrap.scss';
import './app.scss';