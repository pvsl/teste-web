build:
	docker-compose build

up:
	docker-compose up -d

up-non-daemon:
	docker-compose up

start:
	docker-compose start

stop:
	docker-compose stop

restart:
	docker-compose stop && docker-compose start

shell-nginx:
	docker exec -ti nginx-container bash

shell-web:
	docker exec -ti web-container zsh

shell-db:
	docker exec -ti db-container bash

shell-frontend:
	docker exec -ti frontend-container bash

log-nginx:
	docker-compose logs nginx

log-web:
	docker-compose logs web  

log-db:
	docker-compose logs db

collectstatic:
	docker exec web-container /bin/sh -c "python manage.py collectstatic --noinput"  

migrations:
	docker exec web-container /bin/sh -c "python manage.py makemigrations"

migrate:
	docker exec web-container /bin/sh -c "python manage.py migrate"