FROM python:latest
ENV PYTHONUNBUFFERED 1
ENV C_FORCE_ROOT true
ENV DJANGO_STATICFILES_DIRS /opt/django/static-data/
RUN ["apt-get", "update"]
RUN ["apt-get", "install", "-y", "apt-utils"]
RUN ["apt-get", "install", "-y", "zsh"]
RUN wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh || true
RUN mkdir /src
WORKDIR /src
RUN pip install --upgrade pip
ADD ./requirements.pip /src/requirements.pip
RUN pip install -r requirements.pip
ADD ./src /src
RUN python manage.py collectstatic --no-input
CMD gunicorn testeweb.wsgi -b 0.0.0.0:8000 -w 2 --reload